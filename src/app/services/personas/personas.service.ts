import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment as env } from '../../../environments/environment';
import { GlobalService } from '../global/global.service';

interface Persona {
  [index: number]: {
    name: string;
    age: string;
    gender: string;
    document?: string | ArrayBuffer;
  }
}
@Injectable({
  providedIn: 'root'
})

export class PersonasService {

  storageName = env.personas.storageName
  personas: Persona;

  currentId: number;
  currentPersona: any = {};

  constructor(
    private http: HttpClient,
    private globalService: GlobalService
  ) { }

  checkPersonas() {
    return new Promise((res, rej) => {
      if (!localStorage.getItem('PersonasLuis')) {
        this.getPersonas().then(() => {
          res(this.personas);
        })
      } else {
        this.loadPersonas().then(() => {
          res(this.personas);
        });
      }
    })
  }

  getPersonas() {
    return new Promise((res, rej) => {
      this.http.get(env.personas.api).subscribe((data: any) => {
        this.personas = data.results;
        this.savePersonas();
        res(true);
      })
    })
  }

  savePersonas() {
    return new Promise((res, rej) => {
      localStorage.setItem(this.storageName, JSON.stringify(this.personas));
      res(true);
    });
  }

  loadPersonas() {
    return new Promise((res, rej) => {
      const objPersonas = JSON.parse(localStorage.getItem(this.storageName));
      this.personas = objPersonas;
      res(true);
    })
  }

  editPersona(id) {
    this.currentId = id;
    this.currentPersona = this.personas[id];
    this.globalService.formEdit = true;
  }

  saveEditPersona() {
    return new Promise((res, rej) => {
      this.personas[this.currentId] = this.currentPersona;
      this.savePersonas().then(() => {
        this.loadPersonas().then(() => {
          res(true);
        })
      })
    });
  }

  uploadDocumentPersona(e) {
    const reader = new FileReader();

    reader.addEventListener('load', () => {
      this.personas[this.currentId].document = reader.result;
    })
    reader.readAsDataURL(e[0]);
  }

  deleteDocumentPersona() {
    this.personas[this.currentId].document = undefined;
    this.saveEditPersona();
  }
}
