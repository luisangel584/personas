import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global/global.service';
import { PersonasService } from 'src/app/services/personas/personas.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {

  headElements = ['Nombre', 'Edad', 'Sexo', 'Documento'];

  constructor(
    public personasService: PersonasService,
    public globalService: GlobalService
  ) { }

  ngOnInit(): void {
    this.checkInitial();
  }

  checkInitial() {
    this.personasService.checkPersonas();
  }
 
  saveEditPersona() {
    this.personasService.saveEditPersona().then(() => {
      this.globalService.formEdit = false;
    })
  }

  cancel() {
    this.personasService.loadPersonas();
    this.globalService.formEdit = false;
  }

}